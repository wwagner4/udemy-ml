from mnist_helper import train_mnist
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
train_mnist()
