import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import RMSprop
from keras.losses import categorical_crossentropy


# dataset vorbereiten
# generieren von train, val und test daten
x_train = np.random.random((1000, 100))
x_val = np.random.random((100, 100))
x_test = np.random.random((100, 100))

# generieren von train, val und test labels
y_train = np.random.randint(10, size=(1000, 1))
y_val = np.random.randint(10, size=(100, 1))
y_test = np.random.randint(10, size=(100, 1))

# Test ausgabe
print("Shape von x_train: ", x_train.shape)
print("Shape von y_train: ", y_train.shape)

# model definieren
model = Sequential()

# model layers definieren
model.add(Dense(32, input_dim=100))
model.add(Activation('relu'))
model.add(Dense(10))
model.add(Activation('softmax'))

# model kompilieren
rmsprop_opt = RMSprop(lr=0.0001)
model.compile(optimizer=rmsprop_opt, loss=categorical_crossentropy, metrics=['accuracy'])
# model.summary()

# model labels zu one-hot definieren
y_train_one_hot = keras.utils.to_categorical(y_train, num_classes=10)
y_val_one_hot = keras.utils.to_categorical(y_val, num_classes=10)
y_test_one_hot = keras.utils.to_categorical(y_test, num_classes=10)

model.fit(x_train, y_train_one_hot, batch_size=32, epochs=10, validation_data=(x_val, y_val_one_hot), verbose=2)
score = model.evaluate(x_test, y_test_one_hot, batch_size=32, verbose=0)

print(score)
