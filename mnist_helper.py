import base64
import re
from pathlib import Path
import sys
import keras
import matplotlib.pyplot as plt
import numpy as np
import skimage.io
from keras import Model
from keras.datasets import mnist
from keras.layers import Conv2D, Activation, MaxPooling2D, BatchNormalization, Flatten, Dropout
from keras.layers import Dense
from keras.losses import categorical_crossentropy
from keras.models import Sequential
from keras.models import model_from_json
from keras.optimizers import SGD
from skimage import exposure
from skimage.transform import resize
import logging




def prepro_features(x: np.ndarray) -> np.ndarray:
    return x \
               .reshape((x.shape[0], x.shape[1], x.shape[2], 1)) \
               .astype('float') / 255


def model2() -> Model:
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=(1, 28, 28)))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])
    return model


def model1() -> Model:
    learning_rate = 1e-3
    momentum = 0.9

    model = Sequential()
    model.add(Conv2D(filters=32, kernel_size=(3, 3), strides=(1, 1), input_shape=(28, 28, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())

    model.add(Flatten())
    model.add(BatchNormalization())
    model.add(Dense(10))
    model.add(Activation('softmax'))

    model.compile(
        loss=categorical_crossentropy,
        optimizer=SGD(lr=learning_rate, momentum=momentum, nesterov=True),
        metrics=['accuracy'])
    return model


def train_mnist():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    epochs = 5
    batch_size = 128

    x_train = prepro_features(x_train)
    x_test = prepro_features(x_test)

    y_train_one_hot = keras.utils.to_categorical(y_train, num_classes=10)
    y_test_one_hot = keras.utils.to_categorical(y_test, num_classes=10)

    model = model1()
    model.summary()

    history = model.fit(x_train, y_train_one_hot, batch_size=batch_size, epochs=epochs,
                        validation_data=(x_test, y_test_one_hot), verbose=2)

    logging.info(("history: {}".format(history)))

    model_json = model.to_json()
    data_dir = Path("data")
    data_dir.mkdir(parents=True, exist_ok=True)

    struct_file = data_dir / "mnist-model-struct.json"
    with open(struct_file, "w") as json_file:
        json_file.write(model_json)
        logging.info("Saved model to disk {}".format(struct_file))
    # serialize weights to HDF5
    weights_file = data_dir / "mnist-model-weights.h5"
    model.save_weights(weights_file)
    logging.info("Saved weights to disk {}".format(weights_file))


def load_mnist_model() -> Sequential:
    data_dir = Path("data")
    struct_file = data_dir / "mnist-model-struct.json"
    with open(struct_file, 'r') as json_file:
        loaded_model_json = json_file.read()
    model: Sequential = model_from_json(loaded_model_json)
    # load weights into new model
    weights_file = data_dir / "mnist-model-weights.h5"
    model.load_weights(weights_file)
    logging.info("Loaded model from disk. {}".format(struct_file.absolute()))
    logging.info("Loaded model from disk. {}".format(weights_file.absolute()))
    return model


def load_mnist_data() -> np.ndarray:
    (_, _), (x_test, _) = mnist.load_data()
    return prepro_features(x_test[0:10])


def fmt(val: float) -> str:
    if val < 0.001:
        return " -    "
    else:
        return "{:6.4f}".format(val)


def predict_img(img: np.ndarray, model: Sequential):
    # logging.info(fmt_data(img))
    # logging.info(img.shape)
    # logging.info(model)
    return model.predict(img)


def base64_image(b64_url) -> np.ndarray:
    b64_str = re.search("b'.*,(.*)'", b64_url).group(1)
    img = base64.b64decode(b64_str)
    ia = skimage.io.imread(img, plugin='imageio')
    ia1 = resize(ia, (28, 28), anti_aliasing=True)
    # logging.info("image from png shape: {}".format(ia2.shape))
    # logging.info("image from png type: {}".format(type(ia2)))
    ia2 = ia1.mean(axis=2)
    ia3 = exposure.rescale_intensity(ia2)
    # logging.info("ia3 shape: {}".format(ia3.shape))
    # noinspection PyUnresolvedReferences
    return ia3.reshape((1, ia3.shape[0], ia3.shape[1], 1))


def show_image(data: np.ndarray):
    # logging.info("image from png: {}".format(ia4))
    plt.set_cmap('Greys')
    plt.imshow(data)
    plt.show()


def tensorflow_image():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    img1 = x_train[0]
    logging.info("image from keras xtrain shape: {}".format(img1.shape))
    logging.info("image from keras xtrain type: {}".format(type(img1)))
    logging.info("image from keras xtrain: {}".format(img1))
    plt.set_cmap('Greys')
    plt.imshow(img1)
    plt.show()


def fmt_data(data: np.array) -> str:
    def fmt_float(f: float) -> str:
        if f < 0.0001:
            return " .  "
        else:
            return "{:4.2f}".format(f)

    def fmt_line1(line: np.array) -> str:
        return "".join(fmt_float(x) for x in line)

    def fmt_line(line: np.array) -> str:
        return " ".join(fmt_line1(x) for x in line)

    return "\n".join(fmt_line(x) for x in data[0])


def predict_mnist():
    model = load_mnist_model()
    data = load_mnist_data()
    # logging.info(fmt_data(data))
    pred = predict_img(data, model)
    for pre in pred:
        predf = [fmt(f) for f in pre]
        logging.info("pred mnist: {}".format(predf))


def predict_base64():
    model = load_mnist_model()
    b64_url = "b'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAYAAACAvzbMAAAYZElEQVR4Xu3dDay/fT0H8HdISncUUx7KPKZYKsIWSTO3IasmPXlYKUQsyzIxkamFaUp5WFmbyMOiSI2WW8TapMxDIaWV3MUkdUses69+fztd93Xu87+/9/mcc12f87q2s/jf5/v5fT+vz7X/+3/O73dd143iIECAAAECEwI3mlhjCQECBAgQiABxEhAgQIDAlIAAmWKziAABAgQEiHOAAAECBKYEBMgUm0UECBAgIECcAwQIECAwJSBAptgsIkCAAAEB4hwgQIAAgSkBATLFZhEBAgQICBDnAAECBAhMCQiQKTaLCBAgQECAOAcIECBAYEpAgEyxWUSAAAECAsQ5QIAAAQJTAgJkis0iAgQIEBAgzgECBAgQmBIQIFNsFhEgQICAAHEOECBAgMCUgACZYrOIAAECBASIc4AAAQIEpgQEyBSbRQQIECAgQJwDBAgQIDAlIECm2CwiQIAAAQHiHCBAgACBKQEBMsVmEQECBAgIEOcAAQIECEwJCJApNosIECBAQIA4BwgQIEBgSkCATLFZRIAAAQICxDlAgAABAlMCAmSKzSICBAgQECDOAQIECBCYEhAgU2wWESBAgIAAcQ4QIECAwJSAAJlis4gAAQIEBIhzgAABAgSmBATIFJtFBAgQICBAnAMECBAgMCUgQKbYLCJAgAABAeIcIECAAIEpAQEyxWYRAQIECAgQ5wABAgQITAkIkCk2iwgQIEBAgDgHCBAgQGBKQIBMsVlEgAABAgLEOUCAAAECUwICZIrNIgIECBAQIM4BAgQIEJgSECBTbBYRIECAgABxDhAgQIDAlIAAmWKziAABAgQEiHOAAAECBKYEBMgUm0UECBAgIECcAwQIECAwJSBAptgsIkCAAAEB4hwgQIAAgSkBATLFZhEBAgQICBDnAAECBAhMCQiQKTaLCBAgQECAOAcIECBAYEpAgEyxWUSAAAECAsQ5QIAAAQJTAgJkis0iAgQIEBAgzgECBAgQmBIQIFNsFhEgQICAAHEOECBAgMCUgACZYrOIAAECBASIc4AAAQIEpgQEyBSbRQQIECAgQJwDBAgQIDAlIECm2CwiQIAAAQHiHCBAgACBKQEBMsVmEQECBAgIEOcAAQIECEwJCJApNosIECBAQIA4BwgQIEBgSkCATLFZRIAAAQICxDlAgAABAlMCAmSKzSICBAgQECDOAQIECBCYEhAgU2wWESBAgIAAcQ4QIECAwJSAAJlis4gAAQIEBIhzgAABAgSmBATIFJtFBAgQICBAnAMECBAgMCUgQKbYLCJAgAABAeIcIECAAIEpAQEyxWYRAQIECAgQ5wABAgQITAkIkCk2iwgQIEBAgDgHCBAgQGBKQIBMsVlEgAABAgLEOUCAAAECUwICZIrNIgIECBAQIM4BAgQIEJgSECBTbBYRIECAgABxDhAgQIDAlIAAmWKziAABAgQEiHOAAAECBKYEBMgUm0UECBAgIECcAwQIECAwJSBAptgsIkCAAAEB4hzYg8AHJ7lFkg878nX0/797knclecsJzdwkyccl+a8kv5nkL5L8VZL/2QOCPRLYmoAA2dpELs5+PjLJ5yT5piTXHL6OC4grCllGeIwgufT1miR/fQiW/yx8XaUJ7F5AgOx+hLtoYPzL/26HwBih8VlJPnnjOx/hMULlzw9hcilUxk8s79n43m2PwJkICJAzYb5wL/KZh5C465H/Hb+G6nD8W5LXHn4N9vokj0lydYfG9EDg+goIkOsr5vuXAh+U5Mokj05yy8N7DLe+YEyj9x+/YD1rl0AEiJNgRuDOh9C4V5LxNUKk+hhvkr8jyb8c+XpnkvF1myQfmOSVl7GJLzy8aX6rJJ+W5EMuY83lfMsTkjz+cr7R9xDoIiBAukyyvo/x6aWfTDJ+PXXbU3q5NyV5c5L/SPKri3AYQXEpIMb/XfGG9k2T3PEQJCNMxtftD/9744ken5Xk4RPrLCGwSwEBssuxnfmmH5bkJ5KMv3Bnj39P8orDTwmvTjK+xieetniM8BjBcvTrDodw+YATNvziJA9N8rYtNmZPBE5TQICcpmbPWo9I8lNJTvqLc9n9+LTSCIzxdSkwxk8aez6GwQiVhyT59iQ3O6aZP03y9Un+ZM/N2juBkwQEyElCF/u/PzHJd18mweuS/FOSH07ysiRvv8x1e/22OyV5dpK7HNPAu5PcP8mL9tqgfRM4SUCAnCR0Mf/7+DTV05M86DraH+9JjKu5fyvJ723411GVE/yoJM9Mcu9jXuS/k9zcdSOVI1D7PAUEyHnqb/O1x7+of/pw4d/aDt+Q5PuTPPdwLcQ2uzjbXY0PF3zzMS85fjL71LPdjlcjcDYCAuRsnPfyKvdNMj5JNH4CWTvGv7bHeyKOawt8X5IfOAbm2w4fQuBGoJWAAGk1zhvUzHcm+ZHrqPC4JE+6Qa/Qf/EIiqce0+a4dcu4ct1BoI2AAGkzyhvUyPiU1bip4doxLt4bv575pRv0Chdn8SOTPGOl3fEBg3EDSQeBNgICpM0opxq5XZLfTfIJx6x+1SFYLucK76kNNF30o4d7ZC3bG+8tHfdeSVMKbXUWECCdp3tyb+PGgMfdyuN5Sb7xAnwc92Slue8YH2W+x8rS+yX5tbmSVhHYloAA2dY8znI3L08yHsS0doz3Qh57lptp+Fqfm+SqY67e935Iw4FfxJYEyEWcevJFSX5npfXxcKVvPVx5fjFlTrfrcZfep6yU/MPrCO/T3YFqBAoFBEgh7kZLj9tv/OPKbTjGY17HldPP3+i+97qtn0/y4JXNj4/8jutpHAR2KyBAdju66Y2PfxGPfxkvD8+0mCY9ceF4muGnrHyX90NOpPMNWxYQIFuezunv7YGHK8iXlV+Q5D6n/3IqHgTG+yHjppLLY9yufjy10fUhTpVdCgiQXY5tatO3SPJ3Sa5YrB53yB1PEBzXezjqBMbDptZ+ZTU+kTV+EnEQ2J2AANndyKY3/JzDbciXBcZ1CeP6BEe9wPjgwvgAw/Lwfki9vVcoEBAgBagbLPmxScbzOT50sbfxnI7xKxTH2QmMR/OOO/QuD/fLOrsZeKVTEhAgpwS58TI/lGTcy2p5jAckvXfje++2vePeD3lPkk9K8vfdGtZPXwEB0ne2lzobz6x4bZJbLVr93iQjWBxnL+D9kLM394oFAgKkAHVjJZ+8clX5eD75eDN9PBTKcT4Cb0py28VLX53kY85nO16VwPUXECDX32xPK8ZzPd6YZHwC6+jxXYdHz+6pl257/bLDEx2XfX36BX26Y7f5Xoh+BEjvMf9Yku9YtHhNkvFrrXEjRcf5CdzkmEfdPurwOOHz25lXJnCZAgLkMqF2+G3jkz5vW7llyWOSjGBxnL/AS5J88WIbv5zkAee/NTsgcLKAADnZaK/fsfbex9sPz/4YV0A7zl9g7SmQI/Rvc/5bswMCJwsIkJON9vgd49cj43qDGy82772PbU3zs5P80cqWPj/JH2xrq3ZD4NoCAqTnWfGDScbHdI8e41+2d/SAqM0NfNxeZlzoefQYtzwZV6c7CGxaQIBsejxTmxszHc/1WB7fk+SJUxUtqhR4VpKHLV5gPOzrCypfVG0CpyEgQE5DcVs11v5CGs//+Iwk/7CtrdpNkocm+dkViZse8yktaAQ2IyBANjOKU9nIeO/jDSsXoz378BfVqbyIIqcqMJ4TMp4XsjzuneSFp/pKihE4ZQEBcsqg51xu3JDvqSt7GMEybtvu2KbA65KM56QfPX4lyVdvc7t2ReB9AgKk15nwqiR3WbQ0rvkY1344tivw+0nGJ6+OHr+d5MrtbtnOCAiQTufA1yT5uZWGbpfkzZ0abdjLNyR55qKvl65cZNiwdS3tWcBPIHue3vvv/aok91y0472Pfcz3HklettjqK5PcbR/bt8uLKiBAekx+BMcIkOUx/gIafxE5ti0w3v8Y74McPd6a5KO3vW27u+gCAqTHGfCCJF+5aGX82X16tNe+i5sl+deVLscTJN/dvnsN7lZAgOx2dP+/8fEUu79ZaeMrjrld+P477tnBW1Y+fn37Yz7i21NAV7sTECC7G9m1Nvz0JN+y+NPxqZ7xe3XHfgRekWQ87vboca9jfjW5n67stLWAANn3eI97psTDk4wr0h37EXhekvsttvt1x3yybj9d2WlrAQGy7/H+TJJHLFoYj0odVze7cHBfs31xki9dbPm5SR68rzbs9iIJCJB9T/v1ST5x0cK4nmAZKvvu8mLs/jeSjPetjh7PSfK1F6N9Xe5RQIDscWrv2/MDk4x/oS4Pn9zZ50zHLdwfv9j6k5I8bp/t2PVFEBAg+53yrycZN9w7eoz3Pcb7H479CawFyHgmyPhzB4FNCgiQTY7lxE3dOcmrV77rrsf8+YkFfcO5C4xnod9/sQs3VDz3sdjAdQkIkH2eH09L8qjF1n10d5+zvLTrq1eehe5WNPueafvdC5D9jXjMbDwg6iMWW39Qkl/cXzt2fPjJY/wEsjz8BOL02LSAANn0eFY398gkz1j8l/G883HfpPfurx07TrL266sB4ycQp8emBQTIpsezurk/TjLe6zh6jE/vPGF/rdjxQWDtGpDxn/wE4hTZtIAA2fR4rrW5uyd5+cqW75DkL/fVit0eEXh0kqesiPgUltNk0wICZNPjudbmxnscD1j86S8keci+2rDbFYF/TvLhR/78HUluSYrAlgUEyJan8/57u3mSd61sd9yyfdy63bF/gfH44c9LMh5n6/qP/c+zfQcCZD8jfnKSxy62O25lMh5G5CBAgMCZCwiQMyeffsG1eyW9KMmXT1e0kAABAjdAQIDcALwzXvrSJOP5EEeP+yZ5/hnvw8sRIEDg/wQEyH5OhDcm+fjFdu+U5M/204KdEiDQSUCA7GOaVyR558pWx59fs48W7JIAgW4CAmQfE127eeLfrjwLZB/d2CUBAi0EBMg+xvhVh6uSj+52fNTzyn1s3y4JEOgoIED2MdXxZLrlxYIvSfIl+9i+XRIg0FFAgOxjqm9NcuvFVl+48kCpfXRjlwQItBAQINsf4z2TXLWyTTfa2/7s7JBAawEBsv3xHhcgbrS3/dnZIYHWAgJkH+MdP4GMILl0uNHePuZmlwRaCwiQ/Yx3PC97fL3Gjfb2MzQ7JdBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAoIEAKcZUmQIBAZwEB0nm6eiNAgEChgAApxFWaAAECnQUESOfp6o0AAQKFAgKkEFdpAgQIdBYQIJ2nqzcCBAgUCgiQQlylCRAg0FlAgHSert4IECBQKCBACnGVJkCAQGcBAdJ5unojQIBAoYAAKcRVmgABAp0FBEjn6eqNAAEChQICpBBXaQIECHQWECCdp6s3AgQIFAoIkEJcpQkQINBZQIB0nq7eCBAgUCggQApxlSZAgEBnAQHSebp6I0CAQKGAACnEVZoAAQKdBQRI5+nqjQABAoUCAqQQV2kCBAh0FhAgnaerNwIECBQKCJBCXKUJECDQWUCAdJ6u3ggQIFAo8L+PzESgm96pKAAAAABJRU5ErkJggg=='"
    data = base64_image(b64_url)
    # logging.info(fmt_data(data))
    pred = predict_img(data, model)
    for pre in pred:
        predf = [fmt(f) for f in pre]
        logging.info("pred base64: {}".format(predf))


def fmt_pred_html(predictions: list) -> str:
    def bar(val: int) -> str:
        return "".join(['|'] * val)

    def row(n: int, f: float) -> str:
        val = int(f * 100)
        bval = int(f * 70)
        cols = """<td style="width:40px">{:3d}</td>
        <td style="width:40px">{:3d}%</td>
        <td style="text-align: left">{}</td>""".format(n, val, bar(bval))
        return """<tr>
        {}    
        </tr>
        """.format(cols)

    rows = "\n".join([row(n, x) for n, x in enumerate(predictions)])
    return """<table>{}</table>""".format(rows)


if __name__ == '__main__':
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s',
        stream=sys.stdout)
    # train_mnist()
    # logging.info(fmt_pred_html([0.5, 0.2, 0.001, 0.91, 0.001]))
    predict_base64()
    # predict_mnist()
