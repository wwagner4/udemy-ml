import pandas as pd

# Series with default index
# s1 = pd.Series({'Wien': 2000000, 'Klagenfurt': 100000, 'Graz': 300000, 'Linz': 290000})

# ['Wien' 'Klagenfurt' 'Graz' 'Linz'] [2000000  100000  300000  290000]

s1 = pd.Series([2000000, 100000, 300000, 290000],
               index=['Wien', 'Klagenfurt', 'Graz', 'Linz'])

d = s1.describe()



print(d)