FROM python:3.7

RUN pip install pipenv

COPY ./*.py /app/

COPY ./Pipfile* /app/

COPY ./web/* /app/web/
COPY ./data/* /app/data/

WORKDIR /app

RUN pipenv install

EXPOSE 5000

ENV DOCKER_HOST 0.0.0.0

CMD pipenv run python mnist_server.py

