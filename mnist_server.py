import os
import traceback
import warnings

from flask import Flask, request, send_from_directory
import mnist_helper as hlp
import tensorflow as tf
import logging
import sys

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s',
    stream=sys.stdout, )

app: Flask = Flask(__name__)
model = hlp.load_mnist_model()
graph = tf.compat.v1.get_default_graph()


@app.route('/submit/', methods=["POST"])
def submit():
    image = str(request.data)
    return handle_image(image)


@app.route('/favicon.ico')
def favicon():
    return f'data:,'


@app.route('/')
def index():
    return send_from_directory('web', "index.html")


def handle_image(b64_url: str) -> str:
    try:
        with graph.as_default():
            data = hlp.base64_image(b64_url)
            pred = hlp.predict_img(data, model)[0]
            logging.info("predicted: {}".format(pred))
            return hlp.fmt_pred_html(pred)
    except:
        logging.error(traceback.format_exc())
        return "<p>Could not predict</p>"


if __name__ == '__main__':
    warnings.simplefilter(action='ignore', category=FutureWarning)
    host = os.getenv("DOCKER_HOST", "localhost")
    app.run(port=5000, host=host)
